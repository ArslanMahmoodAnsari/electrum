const express = require("express");
const router = express.Router();
const _ = require('lodash');
const {User} = require("../model/user");//for make the user object
const Wallet = require("../model/wallet");
const exec = require('child_process').exec;//for running the bash scritps
// var {authenticate} = require('./../middleware/authMiddleware');
//Create new user - signUp/Register

router.post('/add',(req,res)=>{
  let body = _.pick(req.body, ['email', 'password']);
  let user = new User(body);
      
  user.save().then((user) => { 
    const myShellScript = exec('electrum --testnet createnewaddress');
    myShellScript.stdout.on('data', (address)=>{
      address = address.replace("\n","");//coz at the end of the address '\n' was occuring
      let wallet = new Wallet();
      wallet.userId = user._id;
      wallet.address = address;
      console.log(wallet);
      wallet.save().then((wallet)=>{
        console.log(wallet);
        res.json(wallet);
      }).catch((e) => {
        res.status(400).send(e);
      });
    });
    myShellScript.stderr.on('data', (data)=>{
      console.error(data);
      res.send(data);
    });

  }).catch((e) => {
    res.status(400).send(e);
  });
})

// login the existing user - signIn
router.post('/login', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
  
    User.findByCredentials(body.email, body.password).then((user) => {
        res.json(user);
    }).catch((e) => {
      res.status(400).send();
    });
});

router.post('/searchaddress', (req, res) => {
  
});

// router.post('/listaddress', (req, res) => {
//   var body = _.pick(req.body, ['email', 'password']);
//   User.findByCredentials(body.email, body.password).then((user) => {
//     res.send(user);
//   }).catch((e) => {
//     res.status(400).send();
//   });
// });


router.post('/listentoaddress', (req, res) => {
  var body = _.pick(req.body, ['address', 'url']);
  const myShellScript = exec(`electrum --testnet notify ${body.address} ${body.url}`);
  myShellScript.stdout.on('data', (data)=>{
    console.log(data);
    res.send(data); 
});
  myShellScript.stderr.on('data', (data)=>{
    console.error(data);
    res.send(data);
});
});

router.post('/addressnotify', (req, res) => {
  console.log("-------------------------------------");
  console.log("*************************************");
  console.log("-------------------------------------");
  console.log(req.body);
});



module.exports = router;
