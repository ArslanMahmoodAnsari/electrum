const lastTransaction = require('../model/latestTransection');
const Transaction = require('../model/transaction');
const request = require('request');
const exec = require('child_process').exec;//for running the bash scritps
const Wallet = require('../model/wallet');

let CronJob = require('cron').CronJob;//for scheduling


const job = new CronJob('*/10 * * * * *', function() {
    console.log("*-*-*-*-*-*-*-*-*-*-*-");
    console.log("cron Started");
    console.log("*-*-*-*-*-*-*-*-*-*-*-");
    const myShellScript = exec(`electrum --testnet history`);
    myShellScript.stdout.on('data', (history)=>{
    console.log("Recieved History");
        
        lastTransaction.getLastHeight().then((dbheight)=>{
            console.log("Recieved Height: ",dbheight.lastHeight);
            history = JSON.parse(history) 
            let {transactions} = history;       
            transactions.forEach(({height,txid,value}) => {
                
                if(height > dbheight.lastHeight){
                    console.log("Inside the height check at:" , height);
                    request(`https://api.blockcypher.com/v1/btc/test3/txs/${txid}?limit=50&includeHex=true`, function (error, response, body) {
                        if(!error){ 
                            body = JSON.parse(body);//since the response is in String
                            console.log("Got the API response of address:",body.inputs[0].addresses[0]);
                            Wallet.findAnAddress(body.inputs[0].addresses[0]).then((data)=>{
                                console.log("Found the address in DB");
                                dbheight.lastHeight = height;
                                dbheight.save().then((newHeight)=>{
                                    console.log("Height Updated:",newHeight);
                                    let newTransaction = new Transaction();
                                    newTransaction.txHash = txid;
                                    newTransaction.value = value;
                                    newTransaction.to = body.inputs[0].addresses[0];
                                    newTransaction.from = body.outputs[0].addresses[0];
                                    newTransaction.save().then((transaction)=>{
                                        console.log("transection added");
                                    }).catch((e) => {
                                        console.log(e);
                                    })
                                }).catch((e) => {
                                    console.log(e);
                                })
                            }).catch((e) => {
                                console.log(e);
                            })
                        }
                    });
                }
            });
            // console.log();
        }).catch((e) =>{
            console.log(e);
        })
  });
    myShellScript.stderr.on('data', (data)=>{
      console.error(data);
  });
})

job.start();


