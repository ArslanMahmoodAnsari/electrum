const express = require("express");
const bobyParser = require("body-parser");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 3050;
const User = require("./routes/users");
const Transactions = require('./routes/transactions');

//set up bodyParser
app.use(bobyParser.json());
app.use(bobyParser.urlencoded({extended:true}));

//Routes
app.use('/user',User);

//mongooge connection string
mongoose.promise = global.promise;
mongoose.set('useCreateIndex', true);
mongoose.connect("mongodb://root:toor12@ds035004.mlab.com:35004/electrum", { useNewUrlParser: true, useUnifiedTopology: true }).then(
	() => {
	console.log("DB connected..!!!");
	},
	err => {
	console.log("ERROR IN \"DB CONNECTIVITY\" : "  + err);
});

//running server
app.listen( PORT, () => {
	console.log(`server started on PORT => ${PORT}`);
})
