const mongoose = require('mongoose');
const _ = require('lodash');

let UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique:true
  },
  password: {
    type: String,
    require: true,
  }
});

//An instance Method - for returning only the globle data not the secure data
UserSchema.methods.toJSON = function () {
    var user = this;
    var userObject = user.toObject();
    return _.pick(userObject, ['_id', 'email']);
  };



//An Model Method -  for logging in the user
UserSchema.statics.findByCredentials = function (email, password) {
  var User = this;

  return User.findOne({email}).then((user) => {
    if (!user) {
      return Promise.reject();
    }
    return new Promise((resolve, reject) => {
        if (user.password === password) {
            resolve(user);
        } else {
            reject();
        }
    });
  });
};

UserSchema.methods.addAddress = function (address) {
    var user = this;
    user.publicAddress = address;
    return user.save().then((user)=>{
      return user;
    });
};


let User = mongoose.model('User', UserSchema);

module.exports = {User}
