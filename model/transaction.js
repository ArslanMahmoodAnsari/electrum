const mongoose = require('mongoose');

let TransactionSchema = new mongoose.Schema({
    txHash:{
        type:String,
        required:true
    },
    value:{
        type:Number,
        required:true
    },
    from:{
        type:String,
        required:true
    },
    to:{
        type:String,
        required:true

    }
})

module.exports = mongoose.model('Transaction',TransactionSchema);