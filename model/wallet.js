const mongoose = require('mongoose');

let WalletSchema =  mongoose.Schema({
    userId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User",
        required:true
    },
    address:{
        type:String,
        required:true
    },
    balance:{
        type:Number,
        default:0
    }
})

// add address

WalletSchema.methods.createWallet = function(userId){
    let wallet = new WalletSchema();
    wallet.userId = userId;
    myShellScript.stdout.on('data', (address)=>{
        address = address.replace("\n","");//coz at the end of the address '\n' was occuring
        return wallet.save().then((wallet)=>{
            return wallet;
        })
    });
    myShellScript.stderr.on('data', (data)=>{
    console.error(data);
    res.send(data);
    });

}
// search address
WalletSchema.statics.findAnAddress = async function(address){
    return await this.findOne({address});
}


module.exports = mongoose.model('Wallet', WalletSchema);
