const mongoose = require('mongoose');

let LatestTransectionSchema = mongoose.Schema({
    lastHeight:{
        type:Number,
        require:true,
        default:0
    }
})


// get lastHeight
LatestTransectionSchema.statics.getLastHeight = async function(){
    return await this.findOne({});
}

module.exports = mongoose.model('LatestTransection',LatestTransectionSchema);